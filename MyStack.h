#pragma once
#include<iostream>
#define MAX 5
using namespace std;
class MyStack
{
private:
	int TOP = -1;//top
	int *S;//array
	int capacity;//size
public:
	bool isEmpty();
	bool isFull();
	void push(int);
	void pop();
	void xuat();
	int top();
	int size();
	void swapStack(MyStack &);
	int maxStack();
	int minStack();
	friend bool operator ==(MyStack, MyStack);
	friend bool operator !=(MyStack, MyStack);
	friend bool operator >(MyStack, MyStack);
	friend bool operator <(MyStack, MyStack);
	friend bool operator <=(MyStack, MyStack);
	friend bool operator >=(MyStack, MyStack);
	void sortStack(MyStack &);
	MyStack();
	MyStack(int);
	~MyStack();
};

