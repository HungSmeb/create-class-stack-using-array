#include "MyStack.h"
MyStack::MyStack()
{
    S = new int[capacity];
    S[0] = 0;
    TOP = 0;
}
MyStack::~MyStack()
{
    delete S;
}
MyStack::MyStack(int size)
{
    capacity = size;
    S = new int[capacity];
    TOP;
}
bool MyStack::isEmpty()
{
    if (TOP == -1)
        return true;
    else
        return false;
}
bool MyStack::isFull()
{
    if (TOP == capacity - 1)
        return true;
    else
        return false;
}
void MyStack::push(int value)
{
    if (isFull() == true)
        cout << "\nStack day.Khong the them !";
    else {
        TOP++;
        S[TOP] = value;
    }
}
void MyStack::xuat()
{
    if (isEmpty() == true)
        cout << "Stack dang rong !!!";
    while (!isEmpty()) {
        cout << " " << top();
        pop();
    }
}
void MyStack::pop()
{
    if (isEmpty() == true)
        cout << "\n-1" << endl;
    else
        TOP--;
}
int MyStack::top()
{
    if (isEmpty() == true)
        return -1;
    return S[TOP];
}
int MyStack::size()
{
    return TOP + 1;
}
void MyStack::swapStack(MyStack& s1)
{
    int top;
    int capa;
    int* s;

    top = this->TOP;
    s = this->S;
    capa = this->capacity;

    this->capacity = s1.capacity;
    this->TOP = s1.TOP;
    this->S = s1.S;

    s1.capacity = capa;
    s1.TOP = top;
    s1.S = s;
}

int MyStack::minStack()
{
    MyStack s(MAX);
    int min = top();
    bool KT = true;
    while (!isEmpty()) {
        if (top() < min) {
            min = top();
            KT = true;
        }
        else {
            s.push(top());
            pop();
        }
    }
    if (KT == true) {
        while (!s.isEmpty()) {
            push(s.top());
            s.pop();
        }
        return min;
    }
}
int MyStack::maxStack()
{
    MyStack s(MAX);
    int max = top();
    bool KT = true;
    while (!isEmpty()) {
        if (top() > max) {
            max = top();
            KT = true;
        }
        else {
            s.push(top());
            pop();
        }
    }
    if (KT == true) {
        while (!s.isEmpty()) {
            push(s.top());
            s.pop();
        }
        return max;
    }
}
void MyStack::sortStack(MyStack& s)
{
    MyStack t(MAX);
    while (!s.isEmpty()) {
        int k = s.top();
        s.pop();
        while (!t.isEmpty() && t.top() < k) {
            s.push(t.top());
            t.pop();
        }
        t.push(k);
    }
    cout << "\nStack sorted: ";
    t.xuat();
}
bool operator==(MyStack s1, MyStack s2)
{
    bool KT = false;
    MyStack x(MAX), s(MAX);
    if (s1.size() != s2.size())
        return false;
    else {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() == s2.top())
                KT = true;
            else
                return false;
            s1.pop();
            s2.pop();
        }
    }
    if (KT == true)
        return true;
    else
        return false;
}

bool operator!=(MyStack s1, MyStack s2)
{
    bool KT = false;
    if (s1.size() != s2.size())
        return true;
    else {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() != s2.top())
                KT = true;
            else
                return false;
            s1.pop();
            s2.pop();
        }
    }
    if (KT == true)
        return true;
    else
        return false;
}
bool operator>(MyStack s1, MyStack s2)
{
    bool KT = false;
    MyStack x(MAX), s(MAX);
    if (s1.size() > s2.size())
        return true;
    else {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() > s2.top())
                KT = true;
            s1.pop();
            s2.pop();
        }
    }
    if (KT == true) {
        return true;
    }
    else {
        while (!s.isEmpty() && !x.isEmpty()) {
            s1.push(s.top());
            s2.push(x.top());
            s.pop();
            x.pop();
        }
    }
}

bool operator<(MyStack s1, MyStack s2)
{
    bool KT = false;
    MyStack x(MAX), s(MAX);
    if (s1.size() < s2.size())
        return true;
    else {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() < s2.top())
                KT = true;
            s1.pop();
            s2.pop();
        }
    }
    if (KT == true)
        return true;
    else {
        while (!s.isEmpty() && !x.isEmpty()) {
            s1.push(s.top());
            s2.push(x.top());
            s.pop();
            x.pop();
        }
    }
}
bool operator>=(MyStack s1, MyStack s2)
{
    bool KT = false;
    if (s1.size() != s2.size() || s1.size() == s2.size()) {
        if (s1.top() > s2.top())
            return true;
        else if (s1.top() == s2.top()) {
            while (!s1.isEmpty() && !s2.isEmpty()) {
                if (s1.top() > s2.top())
                    return true;
                else {
                    s1.pop();
                    s2.pop();
                }
            }
        }
        else
            return false;
    }
    else if (s1.size() == s2.size()) {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() == s2.top())
                KT = true;
            else
                return false;
            s1.pop();
            s2.pop();
        }
    }
    return KT;
}
bool operator<=(MyStack s1, MyStack s2)
{
    bool KT = false;
    if (s1.size() != s2.size() || s1.size() == s2.size()) {
        if (s1.top() < s2.top())
            return true;
        else if (s1.top() == s2.top()) {
            while (!s1.isEmpty() && !s2.isEmpty()) {
                if (s1.top() < s2.top())
                    return true;
                else {
                    s1.pop();
                    s2.pop();
                }
            }
        }
        else
            return false;
    }
    else if (s1.size() == s2.size()) {
        while (!s1.isEmpty() && !s2.isEmpty()) {
            if (s1.top() == s2.top())
                KT = true;
            else
                return false;
            s1.pop();
            s2.pop();
        }
    }
    return KT;
}
